package com.mauwahid.tm.travelmgt.domain.api.apimodel.hotel;

import lombok.Data;

@Data
public class HotelImage {

    private String hotelId;
    private String imageCode;
    private String imageURL;


}
