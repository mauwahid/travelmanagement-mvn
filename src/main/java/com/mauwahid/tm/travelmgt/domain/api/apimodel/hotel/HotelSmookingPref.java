package com.mauwahid.tm.travelmgt.domain.api.apimodel.hotel;

public class HotelSmookingPref {

    private String key;

    private String desc;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
